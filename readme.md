# Vanilla JS Snuppitz

## "Паттерны"

### Неймспейс/синглтон с инкапсуляцией внутренних механизмов.

Небольшая вариация на известную тему. Можно вызывать как угодно:
`new Stuff()` или `Stuff`, в любом месте кода, будет корректно.

```javascript

Stuff = (function () {
	var instance;

	var _somePrivateStuff = "I'm private";

	return function StuffConstructor () {
		if (!!instance) {
			return instance;
		}

		this.somePublicStuff = "I'm public";
		this.somePrivateStuffGetter = function () {
			return _somePrivateStuff;
		}

		return (this && this.constructor === StuffConstructor) ?
			instance = this : 
			new StuffConstructor();
	}
}())();

```

### Интерпретатор 

Пример внутри парсера произвольных грамматик. Каждая конструкция может иметь собственный обработчик, точка входа у них одна.

```javascript

// Парсер
TreeParser = {
	parse: function (in) {
		return NodeTree;
	},
	execute: function (nodeTree) {
		return nodeTree.root.execute();
	}
};

// грамматическая единица входного потока (после разбора парсером)
TreeParser.Node = function (type, content, params) {
	nodeType = type;
};

TreeParser.Node.NODE_TYPE = {
	EXPRESSION: 1,
	// ....
	CONDITION: 999
};

TreeParser.Node.prototype.execute = function () {
	return this.execute[this.nodeType]();
};

TreeParser.Node.prototype.execute[TreeParser.Node.NODE_TYPE.EXPRESSION] = function () {
	this.children.each(function () {
		this.execute();
	});
	//...
	return result;
};


TreeParser.Node.prototype.execute[TreeParser.Node.NODE_TYPE.CONDITION] = function () {
	this.children.each(function () {
		this.execute();
	});
	//...
	return result;
};

```

### Таблица индексов

На тот случай, когда на клиенте крутятся толстые, редко обновляемые данные (список задач, пользователей и пр.), с частыми запросами на вывод их в отсортированном виде/или на поиск всякого. Чтобы не гонять сортировку каждый раз - результаты сортировки для нужных свойств хранятся в таблице индексов в формате `индекс в отсортированном списке:индекс в оригинальном списке`. Методы сортировки и выборки - на выбор, свои или из underscore/аналогов. Код упрощен, в продакшне надо расширять и дополнять проверками.

```javascript

Index = function (victim) {
	if (victim instanceof Array) {
		this.indexes = {};
		this.victim = victim;
		// ...
	}
};

//...

Index.prototype.hasTableFor = function (propertyName) {
	// ...
};

Index.prototype.create = function (propertyName) {
	// ...
	this.victim.each(function () {
		this.indexes[this.indexTableNameForProperty(propertyName)].push(this.victim[propertyName]);
	});
};

Index.prototype.update = function (propertyName, comparator) {
	// ...
	this.indexes[this.indexTableNameForProperty(propertyName)].sort(comparator);
};

```

### Bulletproof function

Для данных сомнительного происхождения и/или методов-мутантов. Пример -- вектор произвольной размерности. Бонусом -- покрытие JSDoc'ом для линтера/автодополнения IDE.

```javascript

/*
* @class
* @name Vector
* @property dimensions Размерность вектора
* @param [dimensions = 1] {(Number|Array)} размерность вектора или массив с его координатами
* @param [value = 1] {Number|Function} значение координат вектора (если еще не определены) или порождающая их функция
*/
Vector = function (dimensions, value) {
	if (dimensions instanceof Array) {
		// ...
		this.fillFromArray(dimensions.filterValidNumbers());
	} else {
		// ....
		this.fillFromFunction(value)
	}
}

Vector.prototype = new Array ();

Vector.prototype.fillFromArray = function (arr) {
	// ...
};

Vector.prototype.fillFromFunction = function (func) {
	// ...
}

```